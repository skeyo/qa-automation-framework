import Common.Preparation;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Tests for EzAdmin Login
 */
public class Login extends Preparation {

    final Logger logger = Logger.getLogger(Login.class);

    @Parameters({"url"})
    @Test
    public void initializationBeforeAllTestsInSuite(String url) {
        WebDriver driver = invokeBrowser(url + "/signin");
        logger.info(" Thread: " + Thread.currentThread().getId());
        super.loginToApp(driver);
        //super.setCookiePreparation(driver.manage().getCookieNamed("laravel_session"));
        //super.setUrl(url);
    }


}
