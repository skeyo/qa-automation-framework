package Common;

import DriverSetup.LocalDriverManager;
import Navigation.LoginPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

/**
 * Start browser
 */
public class Preparation {

    private static LoginPage login = new LoginPage();
    private static Cookie cookie;

    final Logger logger = Logger.getLogger(Preparation.class);

    public static Cookie getCookiePreparation() {
        return cookie;
    }

    public static void setCookiePreparation(Cookie cookie) {
        Preparation.cookie = cookie;
    }

    /**
     * Starts browsers in threads
     *
     * @param url webpage url declared in .xml file
     */
    public WebDriver invokeBrowser(String url) {
        WebDriver driver = LocalDriverManager.getDriver();
        driver.get(url);
        return driver;
    }

    public WebDriver loginToApp(WebDriver driver, String username, String password) {
        // Login
        //home.clickOnLogin(driver);
        //login.isLoggedIn(driver);
        login.enterEmail(driver, username);
        login.enterPassword(driver, password);
        login.clickSubmit(driver);
        //login.getError(driver);
        login.isLoggedIn(driver);


        return driver;
    }

    /**
     * Valid login with predefined credentials
     */
    public WebDriver loginToApp(WebDriver driver) {
        return loginToApp(driver, "nikola.josifovic@shiftplanning.com", "123123");
    }

    public WebDriver loginAsOtherUser(WebDriver driver) {
        return loginToApp(driver, "", "");
    }

    /**
     * Logout from application
     *
     * @param driver instance of webdriver
     */
    public void logOutFromApp(WebDriver driver) {
        //login.logOut(driver);
    }



}
