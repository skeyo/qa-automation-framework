#!/bin/bash
# /etc/init.d/xvfb
# version 0.1
#
### BEGIN INIT INFO
# Default-Start:  2 3 4 5
# Default-Stop:   0 1 6
# Description:    Starts the Selenium Hub
### END INIT INFO

#Settings
VERSION="2.48.2"
HUBPATH="/home/hub"
XVFB=`pidof Xvfb`
JAVA=`pidof java`
ME=`whoami`
USERNAME='root'
#2>/dev/null
XVFBARGS="+extension RANDR -screen 0 1920x1200x24 &"
SCREEN="99"

as_user() {
  if [ "$ME" = "$USERNAME" ] ; then
    bash -c "$1"
  else
    su - "$USERNAME" -c "$1"
  fi
}

xvfb_start(){

	echo "Starting Xvfb"
	as_user "Xvfb :${SCREEN} ${XVFBARGS}"
	echo "Xvfb has been started on PID: ${XVFB}"
	sleep 5

	echo "Starting Selenium Hub"
	#-Dwebdriver.chrome.driver=chromedriver -Dwebdriver.chrome.logfile=chromedriver.log -Dwebdriver.chrome.verboseLogging=true
	as_user "screen -h 1024 -dmS Selenium_Hub java -jar ${HUBPATH}/selenium-server-standalone-${VERSION}.jar -role hub -Dwebdriver.chrome.driver=${HUBPATH}/chromedriver -Dwebdriver.chrome.logfile=${HUBPATH}/chromedriver.log -Dwebdriver.chrome.verboseLogging=true -hubConfig ${HUBPATH}/DefaultHub.json"
	sleep 5
	echo "Starting Selenium Node"
	as_user "export DISPLAY=:${SCREEN} && screen -h 1024 -dmS Selenium_Node java -jar ${HUBPATH}/selenium-server-standalone-${VERSION}.jar -role node -Dwebdriver.chrome.driver=${HUBPATH}/chromedriver -Dwebdriver.chrome.logfile=${HUBPATH}/chromedriver.log -Dwebdriver.chrome.verboseLogging=true -nodeConfig ${HUBPATH}/DefaultNode.json"
	echo "Hub and Node has been started: "
	as_user "screen -ls"
	sleep 5
}

xvfb_stop(){
	echo "Killing all Xvfb processes: ${XVFB}"
	as_user "killall Xvfb"
	sleep 2

	echo "Killall java processes: ${JAVA}"
	#as_user "killall java"
	sleep 2
}

xvfb_status(){
	echo "PID of Xvfb is: ${XVFB}"
	echo "Hub and Node screen: "
	as_user "screen -ls"
}

case "$1" in
 	start)
	 xvfb_start
		;;
	stop)
	 xvfb_stop
		;;
	restart)
     xvfb_stop
     xvfb_start
		;;
	status)
	 xvfb_status
		;;
	*)
     echo $"Usage: $0 (start|stop|restart|status)"
     exit 1
		;;
esac

exit 0
