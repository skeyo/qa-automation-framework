package Common;

import DriverSetup.IWaitForElement;
import DriverSetup.WaitForElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class Html5Drag {

    private static final String dragAndDropRawJS = "function simulateDragDrop(a,b){function d(a){var b=new CustomEvent(\"CustomEvent\");return b.initCustomEvent(a,!0,!0,null),b.dataTransfer={data:{},setData:function(a,b){this.data[a]=b},getData:function(a){return this.data[a]}},b}function e(a,b,c){return a.dispatchEvent?a.dispatchEvent(c):a.fireEvent?a.fireEvent(\"on\"+b,c):void 0}var c={DRAG_END:\"dragend\",DRAG_START:\"dragstart\",DROP:\"drop\"},f=d(c.DRAG_START);e(a,c.DRAG_START,f);var g=d(c.DROP);g.dataTransfer=f.dataTransfer,e(b,c.DROP,g);var h=d(c.DRAG_END);h.dataTransfer=f.dataTransfer,e(a,c.DRAG_END,h)}simulateDragDrop(arguments[0],arguments[1]);";
    private static final String loadPageJS = "document&&\"function\"==typeof document.addEventListener&&document.addEventListener(\"DOMContentLoaded\",function(){var e=document.getElementsByTagName(\"body\")[0];e&&(e.innerHTML+='<span id=\"selenium-dom-loaded\">Joske Car</span>')});";
    private static final String deleteDom = "function removeElement(e){e&&e.parentNode&&e.parentNode.removeChild(e)}removeElement(document.getElementById(\"selenium-dom-loaded\"));";

    IWaitForElement iWaitForElement = new WaitForElement();
    final Logger logger = Logger.getLogger(Html5Drag.class);

    public void html5_DragAndDrop(WebDriver driver, WebElement dragFrom, WebElement dragTo) {
        iWaitForElement.waitForElement(driver, dragFrom);
        iWaitForElement.waitForElement(driver, dragTo);
        ((org.openqa.selenium.JavascriptExecutor) driver).executeScript(dragAndDropRawJS, dragFrom, dragTo);
    }

    public void pageLoaded(WebDriver driver){
        Wait<WebDriver> wait = new FluentWait<>(driver);
        wait.until(driver1 -> ((JavascriptExecutor) driver1).executeScript("return document.readyState").equals("complete"));
       // try {
//        ((org.openqa.selenium.JavascriptExecutor)driver).executeAsyncScript(loadPageJS);
//        iWaitForElement.waitForElement(driver, By.id("selenium-dom-loaded"));
//        ((org.openqa.selenium.JavascriptExecutor)driver).executeAsyncScript(deleteDom);
//        } catch (org.openqa.selenium.WebDriverException e) {
//            logger.error("Thread: " + Thread.currentThread().getId() + " - " + "TEST !!!");
//        }
    }
}