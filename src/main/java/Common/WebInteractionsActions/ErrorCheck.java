package Common.WebInteractionsActions;

import Common.IsPresent;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by nikolajosifovic on 3/20/15.
 */
public class ErrorCheck implements IErrorCheck {

    final static Logger logger = Logger.getLogger(ErrorCheck.class);
    IsPresent present = new IsPresent();

    /**
     * Check if error is present and writes error in log
     *
     * @param driver instance of webdriver
     */
    @Override
    public void checkForError(WebDriver driver, By error) {
        try {
            WebElement element = driver.findElement(error);
            if (present.isElementPresent(driver, error)) {
                logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element.getText());
            }
        } catch (org.openqa.selenium.WebDriverException e) {
        }
    }

    /**
     * Check if errors are present and writes errors in log
     *
     * @param driver instance of webdriver
     */
    @Override
    public void checkForTwoErrors(WebDriver driver, By error1, By error2) {
        try {
            WebElement element = driver.findElement(error1);
            if (present.isElementPresent(driver, error1)) {
                logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + " First Error " + element.getText());
            }
        } catch (org.openqa.selenium.WebDriverException e) {
        }
        try {
            WebElement element = driver.findElement(error2);
            if (present.isElementPresent(driver, error2)) {
                logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + " Second Error " + element.getText());
            }
        } catch (org.openqa.selenium.WebDriverException e) {
        }
    }
}
