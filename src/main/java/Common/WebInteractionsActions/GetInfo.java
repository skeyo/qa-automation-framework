package Common.WebInteractionsActions;

import DriverSetup.IWaitForElement;
import DriverSetup.WaitForElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.Beta;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nikolajosifovic on 3/20/15.
 */
public class GetInfo implements IGetInfo {

    final static Logger logger = Logger.getLogger(GetInfo.class);
    private IWaitForElement waitForElement = new WaitForElement();

    @Override
    public String getText(WebDriver driver, By element){
        waitForElement.waitForElement(driver, element);
        String s = driver.findElement(element).getText();
        logger.info(" Thread: " + Thread.currentThread().getId() + " - Text of the element is: " + s );
        return s;
    }

    @Override
    public String getText(WebDriver driver, WebElement element){
        waitForElement.waitForElement(driver, element);
        String s = element.getText();
        logger.info(" Thread: " + Thread.currentThread().getId() + " - Text of the element is: " + s );
        return s;
    }

    /**
     * Clicks on element and return number of unordered list items
     * Returns 0 if can't get number of elements
     *
     * @param driver  instance of webdriver
     * @param element location of element
     */
    @Override
    public int numberOfItemsInElement(WebDriver driver, By element) {
        try {
            waitForElement.waitForElement(driver, element);
            List<WebElement> list = driver.findElements(element);
            return list.size();
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
            return 0;
        }
    }

    /**
     * Check if table is empty by comparing text in footer with string text
     *
     * @param driver                       instance of webdriver
     * @param text                         text to compare with footer table information
     * @param dataTablesEmptyClassLocation information in footer of table if table is empty
     */
    @Override
    public void isTableEmpty(WebDriver driver, String text, By dataTablesEmptyClassLocation) {
        waitForElement.waitForText(driver, text, dataTablesEmptyClassLocation);
        if (driver.findElement(dataTablesEmptyClassLocation).getText().equalsIgnoreCase(text)) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + dataTablesEmptyClassLocation);
            driver.close();
        }
    }

    /**
     * Get all elements in list and prints them. Used for logging of encounters
     *
     * @param driver          instance of webdriver
     * @param elementLocation location of div that holds elements to be printed
     * @return All elements text
     */
    @Override
    @Beta
    public String addAllElementsToList(WebDriver driver, String message, By elementLocation) {
        waitForElement.waitForElement(driver, elementLocation);

        String result = "";
        WebElement element = driver.findElement(elementLocation);
        List<WebElement> list = element.findElements(By.xpath("div"));
        if (list.size() > 0) {
            for (WebElement current : list) result += "\n---------------------------\n" + current.getText();
            return result;
        } else return result + message;
    }

    /**
     * Checking if element is present in div. Used to check if encounter is available
     *
     * @param driver          instance of webdriver
     * @param text            Text to find if is present in div that is searched
     * @param elementLocation location of main div that holds all elements
     * @return true if text is found
     */
    @Override
    @Beta
    public boolean isElementInClassPresent(WebDriver driver, String text, By elementLocation) {
        waitForElement.waitForElement(driver, elementLocation);
        try {
            WebElement element = driver.findElement(elementLocation);
            List<WebElement> list = element.findElements(By.xpath("div"));
            for (WebElement current : list)
                if (current.getText().contains(text)) return true;
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + elementLocation);
        }
        return false;
    }

    /**
     * Used for logger to log text from element
     *
     * @param driver               instance of webdriver
     * @param textToBePresent      text that should be displayed in element
     * @param whatAreYouLogging    message to log
     * @param notificationLocation location of element that holds text
     */
    @Override
    @Beta
    public void logNotification(WebDriver driver, String textToBePresent, String whatAreYouLogging, By notificationLocation) {
        try {
            waitForElement.waitForText(driver, textToBePresent, notificationLocation);
            logger.info("Thread: " + Thread.currentThread().getId() + " - " + whatAreYouLogging + driver.findElement(notificationLocation).getText());
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + notificationLocation);
        }
    }

    /**
     * Get list of web elements of encounter with specific names
     *
     * @param driver          instance of webdriver
     * @param encounterName   desired encounter name
     * @param elementLocation location of encounter holder
     * @return list of web elements with desired encounter
     */
    @Override
    @Beta
    public List<WebElement> allEncountersWithSpecificName(WebDriver driver, String encounterName, By elementLocation) {
        WebElement encounterHolder = driver.findElement(elementLocation);
        List<WebElement> encountersBeforeSearch = encounterHolder.findElements(By.cssSelector(".event-body"));
        List<WebElement> allEncountersAfterSearch = encountersBeforeSearch.stream()
                .filter(current -> current.getText().contains(encounterName))
                .collect(Collectors.toList());

//        List<WebElement> allEncountersAfterSearch = new LinkedList<>();
//        for (WebElement webElement : encountersBeforeSearch) {
//            if(webElement.getText().contains(encounterName)){
//                allEncountersAfterSearch.add(webElement);
//            }
//        }
        return allEncountersAfterSearch;
    }

    @Override
    @Beta
    public List<String> dataId(WebDriver driver, List<WebElement> allEncounters) {
        List<String> dataIDs = allEncounters.stream().map(allEncounter -> allEncounter.getAttribute("data-id"))
                .collect(Collectors.toCollection(() -> new LinkedList<>()));
        return dataIDs;
    }
}
