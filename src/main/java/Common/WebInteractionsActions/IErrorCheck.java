package Common.WebInteractionsActions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by nikolajosifovic on 3/20/15.
 */
public interface IErrorCheck {
    void checkForError(WebDriver driver, By error);

    void checkForTwoErrors(WebDriver driver, By error1, By error2);
}
