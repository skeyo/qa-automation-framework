package Common.WebInteractionsActions;

import DriverSetup.IWaitForElement;
import DriverSetup.WaitForElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by nikolajosifovic on 3/20/15.
 */
public class SendKeys implements ISendKeys {

    final static Logger logger = Logger.getLogger(SendKeys.class);
    IWaitForElement iWaitForElement = new WaitForElement();

    /**
     * Send keys to element
     *
     * @param driver     instance of webdriver
     * @param whatToSend string that should be sent to element
     * @param element    location of element
     */
    @Override
    public void sendString(WebDriver driver, String whatToSend, By element) {
        try {
            iWaitForElement.waitForElement(driver, element);
            driver.findElement(element).click();
            driver.findElement(element).sendKeys(whatToSend);
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Send keys to element
     *
     * @param driver     instance of webdriver
     * @param whatToSend string that should be sent to element
     * @param element    location of element
     */
    @Override
    public void sendString(WebDriver driver, String whatToSend, WebElement element) {
        try {
            iWaitForElement.waitForElement(driver, element);
            element.click();
            element.sendKeys(whatToSend);
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Send keys to element and after use Enter on keyboard
     *
     * @param driver     instance of webdriver
     * @param whatToSend string that should be sent to element
     * @param element    location of element
     */
    @Override
    public void sendStringWithEnter(WebDriver driver, String whatToSend, By element) {
        try {
            iWaitForElement.waitForElement(driver, element);
            driver.findElement(element).sendKeys(whatToSend);
            driver.findElement(element).sendKeys(Keys.RETURN);
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * @param driver          instance of webdriver
     * @param whatToSend      string for input search
     * @param elementForFocus element that should be focused
     */
    @Override
    public void focusElementSendString(WebDriver driver, String whatToSend, By elementForFocus) {
        iWaitForElement.waitForElement(driver, elementForFocus);
        WebElement element = driver.findElement(elementForFocus);
        try {
            new Actions(driver).moveToElement(element).sendKeys(whatToSend).perform();
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Select first item from search
     *
     * @param driver          instance of webdriver
     * @param elementForFocus element that should be focused
     */
    @Override
    public void focusElementSendEnter(WebDriver driver, By elementForFocus) {
        iWaitForElement.waitForElement(driver, elementForFocus);
        WebElement element = driver.findElement(elementForFocus);
        try {
            new Actions(driver).moveToElement(element).sendKeys(Keys.RETURN).perform();
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Entering search into search input field and waiting for search to be done before sending enter key*
     * Only handles searches where Highlighter is present e.g. server searches
     *
     * @param driver            instance of webdriver
     * @param searchPhrase      search phrase to be searched for
     * @param searchInputField  location of search input filed
     * @param searchHighlighted location of loader
     */
    @Beta
    @Override
    public void waitForSearchToFinishAndSendEnter(WebDriver driver, String searchPhrase, By searchInputField,
                                                  By searchHighlighted) {
        try {
            iWaitForElement.waitForElement(driver, searchInputField);
            driver.findElement(searchInputField).sendKeys(searchPhrase);
            iWaitForElement.waitForElement(driver, searchHighlighted);
            new Actions(driver).sendKeys(Keys.RETURN).perform();
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + searchHighlighted);
        }
    }

    /**
     * Entering search into search input field and waiting for search to be done before sending enter key*
     * Only handles searches where Highlighter is present e.g. server searches
     *
     * @param driver            instance of webdriver
     * @param searchPhrase      search phrase to be searched for
     * @param searchInputField  location of search input filed
     * @param searchHighlighted location of loader
     */
    @Beta
    @Override
    public void waitForSearchToFinishAndSendEnter(WebDriver driver, String searchPhrase, WebElement searchInputField,
                                                  By searchHighlighted) {
        try {
            iWaitForElement.waitForElement(driver, searchInputField);
            searchInputField.sendKeys(searchPhrase);
            iWaitForElement.waitForElement(driver, searchHighlighted);
            new Actions(driver).sendKeys(Keys.RETURN).perform();
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + searchHighlighted);
        }
    }

    /**
     * Uploads file
     *
     * @param filePath     is String = path of the file + file name + extension
     * @param element      is <input type="file" name="file">
     * @param inputElement is whole <div></div>
     * @param driver       instance of webdriver
     */
    @Override
    public void uploadFile(WebDriver driver, String filePath, By element, By inputElement) {
        try {
            iWaitForElement.waitForElement(driver, inputElement);
            driver.findElement(element).sendKeys(filePath);
        } catch (org.openqa.selenium.WebDriverException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    @Override
    public String getStringFromElement(WebDriver driver, By elementLocation) {
        iWaitForElement.waitForElement(driver, elementLocation);
        WebElement element = driver.findElement(elementLocation);
        return element.getText();
    }

}
