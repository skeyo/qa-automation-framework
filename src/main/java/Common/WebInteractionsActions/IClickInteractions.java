package Common.WebInteractionsActions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by nikolajosifovic on 3/19/15.
 */
public interface IClickInteractions {

    void clickOnElement(WebDriver driver, By element);

    void clickOnElement(WebDriver driver, WebElement element);

    void clickClear(WebDriver driver, By element);

    void clickClear(WebDriver driver, WebElement element);

    void clickClearToSpecificState(WebDriver driver, String resultOfClear, By inputFiled);

    void clickClearToSpecificState(WebDriver driver, String resultOfClear, WebElement inputFiled);

    void focusElementClick(WebDriver driver, By elementForFocus);

    void focusElementDoubleClick(WebDriver driver, By elementForFocus);

    void focusElementDoubleClick(WebDriver driver, WebElement elementForFocus);

    void clickOnFirstElementFromTable(WebDriver driver, String textOfElement, By element);

    void clickOnElementWithText(WebDriver driver, String text, By elementLocation);

    void clickElementFromDropDownByVisibleText(WebDriver driver, String whatToSelect, By element);

    void clickElementFromDropDown(WebDriver driver, By element, By dropDownElement);

    void dragAndDrop(WebDriver driver, By from, By to);
}
