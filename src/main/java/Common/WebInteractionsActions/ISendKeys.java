package Common.WebInteractionsActions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by nikolajosifovic on 3/20/15.
 */
public interface ISendKeys {
    void sendString(WebDriver driver, String whatToSend, By element);

    void sendString(WebDriver driver, String whatToSend, WebElement element);

    void sendStringWithEnter(WebDriver driver, String whatToSend, By element);

    void focusElementSendString(WebDriver driver, String whatToSend, By elementForFocus);

    void focusElementSendEnter(WebDriver driver, By elementForFocus);

    void waitForSearchToFinishAndSendEnter(WebDriver driver, String searchPhrase, By searchInputField,
                                           By searchHighlighted);

    void waitForSearchToFinishAndSendEnter(WebDriver driver, String searchPhrase, WebElement searchInputField,
                                           By searchHighlighted);

    void uploadFile(WebDriver driver, String filePath, By element, By inputElement);

    String getStringFromElement(WebDriver driver, By elementLocation);
}
