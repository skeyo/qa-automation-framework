package Common.WebInteractionsActions;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by nikol on 11/26/2015.
 */
public class CommonInteractions {

    final static Logger logger = Logger.getLogger(CommonInteractions.class);
    private IClickInteractions clickInteractions = new ClickInteractions();
    private ISendKeys sendKeys = new SendKeys();
    private IGetInfo getInfo = new GetInfo();

    public void clickSendKeys(WebDriver driver, String input, By element){
        //getInfo.getText(driver, element);
        clickInteractions.clickClear(driver, element);
        sendKeys.sendString(driver, input, element);
        logger.info(" Thread: " + Thread.currentThread().getId() + " - Clicking On location - " + element +
                " - Entering: " + input);
    }
}
