package Common.WebInteractionsActions;

import org.openqa.selenium.Beta;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by nikolajosifovic on 3/20/15.
 */
public interface IGetInfo {

    String getText(WebDriver driver, By element);

    String getText(WebDriver driver, WebElement element);

    int numberOfItemsInElement(WebDriver driver, By element);

    void isTableEmpty(WebDriver driver, String text, By dataTablesEmptyClassLocation);

    @Beta
    String addAllElementsToList(WebDriver driver, String message, By elementLocation);

    @Beta
    boolean isElementInClassPresent(WebDriver driver, String text, By elementLocation);

    @Beta
    void logNotification(WebDriver driver, String textToBePresent, String whatAreYouLogging, By notificationLocation);

    @Beta
    List<WebElement> allEncountersWithSpecificName(WebDriver driver, String encounterName, By elementLocation);

    @Beta
    List<String> dataId(WebDriver driver, List<WebElement> elements);
}
