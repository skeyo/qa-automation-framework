package Common.WebInteractionsActions;

import Common.IsPresent;
import DriverSetup.IWaitForElement;
import DriverSetup.WaitForElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by nikolajosifovic on 3/19/15.
 */
public class ClickInteractions implements IClickInteractions {

    final static Logger logger = Logger.getLogger(ClickInteractions.class);
    IWaitForElement iWaitForElement = new WaitForElement();
    IsPresent present = new IsPresent();

    /**
     * Clicks on element
     *
     * @param driver  instance of webdriver
     * @param element location of element
     */
    @Override
    public void clickOnElement(WebDriver driver, By element) {
        try {
            iWaitForElement.waitForElement(driver, element);
            driver.findElement(element).click();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Clicks on element
     *
     * @param driver  instance of webdriver
     * @param element location of element
     */
    @Override
    public void clickOnElement(WebDriver driver, WebElement element) {
        try {
            iWaitForElement.waitForElement(driver, element);
            element.click();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Click on field and clears any input
     *
     * @param driver  instance of webdriver
     * @param element location of element
     */
    @Override
    public void clickClear(WebDriver driver, By element) {
        try {
            iWaitForElement.waitForElement(driver, element);
            driver.findElement(element).click();
            driver.findElement(element).clear();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Click on field and clears any input
     *
     * @param driver  instance of webdriver
     * @param element location of element
     */
    @Override
    public void clickClear(WebDriver driver, WebElement element) {
        try {
            iWaitForElement.waitForElement(driver, element);
            element.click();
            element.clear();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Select and click on input field and go to beginning of input clear everything until input is equal to resultOfClear
     *
     * @param driver        instance of webdriver
     * @param inputFiled    location of input field
     * @param resultOfClear how string should look like after input is cleared eg. 0.00 or ""
     */
    @Override
    public void clickClearToSpecificState(WebDriver driver, String resultOfClear, By inputFiled) {
        try {
            iWaitForElement.waitForElement(driver, inputFiled);
            driver.findElement(inputFiled).click();
            WebElement element = driver.findElement(inputFiled);
            element.sendKeys(Keys.END);
            while (!element.getAttribute("value").equalsIgnoreCase(resultOfClear)) {
                // "\u0008" - is backspace char
                element.sendKeys("\u0008");
            }
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + inputFiled);
        }
    }

    /**
     * Select and click on input field and go to beginning of input clear everything until input is equal to resultOfClear
     *
     * @param driver        instance of webdriver
     * @param inputFiled    location of input field
     * @param resultOfClear how string should look like after input is cleared eg. 0.00 or ""
     */
    @Override
    public void clickClearToSpecificState(WebDriver driver, String resultOfClear, WebElement inputFiled) {
        try {
            iWaitForElement.waitForElement(driver, inputFiled);
            inputFiled.click();
            inputFiled.sendKeys(Keys.END);
            while (!inputFiled.getAttribute("value").equalsIgnoreCase(resultOfClear)) {
                // "\u0008" - is backspace char
                inputFiled.sendKeys("\u0008");
            }
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + inputFiled);
        }
    }

    /**
     * Focus on element and click
     *
     * @param driver instance of webdriver
     * @throws org.openqa.selenium.NoSuchElementException
     */
    @Override
    public void focusElementClick(WebDriver driver, By elementForFocus) {
        iWaitForElement.waitForElement(driver, elementForFocus);
        WebElement element = driver.findElement(elementForFocus);
        try {
            new Actions(driver).moveToElement(element).click().build().perform();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Focus on element and click twice
     *
     * @param driver instance of webdriver
     * @throws org.openqa.selenium.NoSuchElementException
     */
    @Override
    public void focusElementDoubleClick(WebDriver driver, By elementForFocus) {
        iWaitForElement.waitForElement(driver, elementForFocus);
        WebElement element = driver.findElement(elementForFocus);
        try {
            new Actions(driver).moveToElement(element).click().click().build().perform();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    @Override
    public void focusElementDoubleClick(WebDriver driver, WebElement elementForFocus) {
        iWaitForElement.waitForElement(driver, elementForFocus);
        //Actions action = new Actions(driver);
        //WebElement element = driver.findElement(elementForFocus);
        try {
            new Actions(driver).doubleClick(elementForFocus).perform();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + elementForFocus);
        }
    }

    /**
     * Click on first element from table
     *
     * @param driver  instance of webdriver
     * @param element location of table
     * @throws org.openqa.selenium.NoSuchElementException
     */
    @Override
    public void clickOnFirstElementFromTable(WebDriver driver, String textOfElement, By element) {
        iWaitForElement.waitForElement(driver, element);
        WebElement baseTable = driver.findElement(element);
        List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));
        iWaitForElement.waitForText(driver, textOfElement, element);
        try {
            new Actions(driver).moveToElement(tableRows.get(1)).click().build().perform();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Select first element that contains text
     *
     * @param driver          instance of webdriver
     * @param text            text to find
     * @param elementLocation location of the element where to search for text
     */
    @Override
    public void clickOnElementWithText(WebDriver driver, String text, By elementLocation) {
        iWaitForElement.waitForElement(driver, elementLocation);
        try {
            Actions action = new Actions(driver);
            WebElement element = driver.findElement(elementLocation);
            List<WebElement> list = element.findElements(By.xpath("div"));
            /***
             * For debugging
             ***
             for(WebElement current: list){
             //System.out.println("----------------"+current.getText()+"----------");
             if(current.getText().contains(text)){
             //System.out.println(current.getAttribute("event_id"));
             System.out.println("----------------"+current.getText()+"----------");
             action.doubleClick(current).perform();
             }
             }
             ***/
            list.stream().filter(current -> current.getText().contains(text)).forEach(current ->
                    action.doubleClick(current).perform());
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + elementLocation);
        }
    }

    /**
     * Select element by visible text
     *
     * @param driver       instance of webdriver
     * @param whatToSelect string what to select
     * @param element      location of dropdown menu
     */
    @Override
    public void clickElementFromDropDownByVisibleText(WebDriver driver, String whatToSelect, By element) {
        try {
            iWaitForElement.waitForElement(driver, element);
            Select visibleText = new Select(driver.findElement(element));
            visibleText.selectByVisibleText(whatToSelect);
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Click on dropdown, check if dropdown is shown and select element from it
     *
     * @param driver          instance of webdriver
     * @param element         location of dropdown menu
     * @param dropDownElement element that should be selected
     * @throws java.util.NoSuchElementException*
     */
    @Override
    public void clickElementFromDropDown(WebDriver driver, By element, By dropDownElement) {
        iWaitForElement.waitForElement(driver, element);
        WebElement dropDown = driver.findElement(element);
        try {
            new Actions(driver).moveToElement(dropDown).click().build().perform();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
        if (!(present.isElementPresent(driver, dropDownElement))) {
            try {
                new Actions(driver).moveToElement(dropDown).click().build().perform();
            } catch (NoSuchElementException | StaleElementReferenceException e) {
                logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + dropDown);
            }
        }
        if (present.isElementPresent(driver, dropDownElement)) {
            try {
                new Actions(driver).moveToElement(dropDown).build().perform();
            } catch (NoSuchElementException | StaleElementReferenceException e) {
                logger.error(" - Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + dropDown);
            }
        }
    }

    /**
     * Drag and drop element
     *
     * @param driver instance of webdriver
     * @param from   location of element that needs to be dragged
     * @param to     location of element that needs to be dragged to
     */
    @Beta
    @Override
    public void dragAndDrop(WebDriver driver, By from, By to) {
        iWaitForElement.waitForElement(driver, from);
        WebElement source = driver.findElement(from);
        WebElement target = driver.findElement(to);
        (new Actions(driver)).dragAndDrop(source, target).perform();
    }
}
