package Common.WebInteractionsActions;

import org.openqa.selenium.WebElement;

/**
 * Created by skeyo42gmailcom on 7/14/15.
 */
public class WebElementWrapper {
    private WebElement webElement;

    private String textToCompare;

    public WebElementWrapper(WebElement webElement, String textToCompare) {
        this.webElement = webElement;
        this.textToCompare = textToCompare;
    }

    public WebElement getWebElement() {
        return webElement;
    }

    public void setWebElement(WebElement webElement) {
        this.webElement = webElement;
    }

    public String getTextToCompare() {
        return textToCompare;
    }

    public void setTextToCompare(String textToCompare) {
        this.textToCompare = textToCompare;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WebElementWrapper)) {
            return false;
        }
        WebElementWrapper webElementToCompare = (WebElementWrapper) obj;

        return this.textToCompare.equals(webElementToCompare.getTextToCompare());
    }
}
