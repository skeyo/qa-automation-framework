package Common.APIInteractions;


import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.RedirectConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.config.SessionConfig;
import com.jayway.restassured.filter.Filter;
import com.jayway.restassured.filter.FilterContext;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.FilterableRequestSpecification;
import com.jayway.restassured.specification.FilterableResponseSpecification;
import org.hamcrest.Matchers;
import org.testng.Assert;


import java.util.Map;

import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.given;

/**
 * Created by skeyo42gmailcom on 10/26/15.
 */
public class AccountHumanitySignIn {

    private Map<String, String> loginCredentials = ImmutableMap.of(
            "email", "nikola.josifovic@shiftplanning.com",
            "password", "123123"
    );

    private Map<String, String> nikolaCredentials = ImmutableMap.of(
            "client_id", "5016871371800576",
            "redirect_uri", "https://app.humanity.com/",
            "company_id", "5779978008395776",
            //"scope", "5749762930245632",
            "response_type", "token"
            //"state","1448993392082Z5dHEtItsPwCMc9gu7RaI10rSM10bnMhFE8wtSY5rXi2lzbX",
    );



    public Response accountsHumanitySignInPost(String cooke){

        Response signInPost = given()
                .log().all()
                .formParameters(loginCredentials)
                .cookie(cooke)
                .when()
                .expect()
                .statusCode(200)
                .post("https://accounts.humanity.com/signin")
                .then()
                .contentType(ContentType.JSON)
                .extract()
                .response();



        signInPost.prettyPrint();
        return signInPost;
    }

    public Response test(){

        RestAssured.config = RestAssured.config().redirect(RedirectConfig.redirectConfig().followRedirects(true));

        Response test = given()
                .redirects()
                .follow(true)
                .log().all()
                .formParameters(loginCredentials)
                .when()
                .expect()
                .statusCode(200)
                .post("https://accounts.humanity.com/signin")
                .then()
                .contentType(ContentType.JSON)
                //.body("redirectUrl", org.hamcrest.Matchers.hasItem("company_id"))
                .extract()
                .response();
        //System.out.println("-------------------"+ test.getCookie("PHPSESSID"));

        String redirectUri = test.path("redirectUrl");

        test.prettyPeek();

        System.out.println("---------------------------------------------------"+redirectUri);

        return test;
    }

    public Response login(){

        Response test2 = given()
                //.log().all()
                .formParameters(loginCredentials)
                //.basic()
                //.accept(ContentType.TEXT)
                .post("https://accounts.humanity.com/signin")
                .then()
                .contentType(ContentType.JSON)
                .extract()
                .response();

        //Assert.assertEquals(test2.getStatusCode(), Matchers.equalTo(200));

        Assert.assertEquals(test2.getStatusCode(), 200);

        //test2.prettyPeek();

        //System.out.println(test2.getStatusCode());

        return test2;
    }

    public Response fallowRedirect(Response response){
        String redirectUrl = response.path("redirectUrl");
        String phpSessionId = response.cookie("PHPSESSID");

        Response redirect = given()
                //.log().all()
                .cookie(phpSessionId)
                .redirects()
                .follow(true)
                .get(redirectUrl)
                .thenReturn();
                //.then()
                //.extract()
                //.response();
                //.prettyPeek();

        System.out.println(redirect.cookie("PHPSESSID"));

        Assert.assertEquals(redirect.getStatusCode(), 200);
        //System.out.println(redirect.getSessionId());

        return redirect;
    }

    public Response getOauth(Response response){
        String phpSessionId = response.cookie("PHPSESSID");

        Response getOauth = given()
                .log().all()
                .cookie(phpSessionId)
                .redirects()
                .follow(true)
                .formParameters(nikolaCredentials)
                .get("https://accounts.humanity.com/oauth2/authorize")
                .prettyPeek();

        return getOauth;
    }


    public String accountsHumanitySignInGet(){

        Response signInGet = given()
                .when()
                .expect()
                .statusCode(200)
                .get("https://accounts.humanity.com/signin")
                .then()
                .contentType(ContentType.HTML)
                .extract()
                .response();

        //login.prettyPrint();

        String sessionId = signInGet.getCookie("PHPSESSID");

//        System.out.println("-------------------------------------------------------------------------------------------------------"+sessionId);

        return sessionId;
    }

    public static Filter sign(final String accessToken) {
        return new Filter() {
            @Override
            public Response filter(FilterableRequestSpecification requestSpec,
                                   FilterableResponseSpecification responseSpec,
                                   FilterContext ctx) {
                requestSpec.header("Authorization", String.format("Bearer %s",
                        accessToken));
                return ctx.next(requestSpec, responseSpec);
            }
        };
    }

}
