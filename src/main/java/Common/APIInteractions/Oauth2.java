package Common.APIInteractions;

import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.authentication.OAuth2Scheme;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.internal.UriValidator;
import com.jayway.restassured.internal.http.AuthConfig;
import com.jayway.restassured.response.Response;

import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by nikol on 12/1/2015.
 */
public class Oauth2 {

    private Map<String, String> nikolaCredentials = ImmutableMap.of(
            "client_id", "5016871371800576",
            "redirect_uri", "https://app.humanity.com/",
            "company_id", "5779978008395776",
            //"scope", "5749762930245632",
            "response_type", "token"
            //"state","1448993392082Z5dHEtItsPwCMc9gu7RaI10rSM10bnMhFE8wtSY5rXi2lzbX",
    );

    private Map<String, String> header = ImmutableMap.of(
            "referer", "https://app.humanity.com/",
            "host", "accounts.humanity.com"
    );

    public void authorizeGet(String cooke){

        Response signInPost = given()
                .log().all()
                .formParameters(nikolaCredentials)
                //.headers(header)
                .cookie(cooke)
                .when()
                .expect()
                //.statusCode(302)
                .get("https://accounts.humanity.com/oauth2/authorize")
                .then()
                .contentType(ContentType.HTML)
                .extract()
                .response();

        signInPost.prettyPrint();
    }

    public Response appHumanityGet(String cooke){

        Response getApp = given()
                //.formParameters(nikolaCredentials)
                //.headers(header)
                .cookie(cooke)
                .when()
                .expect()
                .statusCode(200)
                .get("https://app.humanity.com/")
                .then()
                .contentType(ContentType.HTML)
                .extract()
                .response();

       //getApp.prettyPrint();
                return  getApp;
    }
}
