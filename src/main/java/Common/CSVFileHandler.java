package Common;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


/**
 * Saves List to file
 * Compare two lists
 */
public class CSVFileHandler {

    final static Logger logger = Logger.getLogger(CSVFileHandler.class);
    private static final String FILE_EXTENSION = ".csv";
    private static ResourceBundle folders = ResourceBundle.getBundle("folders", Locale.US);
    private static String absoluteFilePath;
    private static ICsvMapReader mapReader = null;
    private static List<String> toList = new ArrayList<>();
    private static Map<String, Object> map;
    private String uploadDirectoryPath;
    private File file;

    public CSVFileHandler() {
        uploadDirectoryPath = this.folders.getString("uploadFileDirectory");
        file = new File(uploadDirectoryPath);
        absoluteFilePath = file.getAbsolutePath();
    }

    public static Set<String> claParser(String fileName) {

        String csvFile = absoluteFilePath + File.separator + fileName + FILE_EXTENSION;
        Set<String> toSet = new HashSet<>();
        final CellProcessor[] processors = new CellProcessor[]{
                new NotNull()};  // CLA number

        try {
            mapReader = new CsvMapReader(new FileReader(csvFile), CsvPreference.STANDARD_PREFERENCE);
            final String[] header = mapReader.getHeader(true);

            while ((map = mapReader.read(header, processors)) != null) {
                //System.out.println(String.format("rowNo=%s", mapReader.getRowNumber()));
                toSet.add(String.format("%s", map));
            }
        } catch (Exception e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + mapReader + "IO Exception");
        } finally {
            if (mapReader != null) {
                try {
                    mapReader.close();
                } catch (IOException e) {
                    logger.error("Thread: " + Thread.currentThread().getId() + " - Could not close: " + mapReader);
                }
            }
        }
        return toSet;
    }

    public static void claWriter(List<String> toFile, String fileName) throws IOException {

        String csvFile = absoluteFilePath + File.separator + fileName + FILE_EXTENSION;

        ICsvListWriter listWriter = null;
        try {
            listWriter = new CsvListWriter(new FileWriter(csvFile),
                    CsvPreference.STANDARD_PREFERENCE);

            //final CellProcessor[] processors = new CellProcessor[]();  // Line number
            final String[] header = new String[]{"ClaNumber"};

            // write the header
            listWriter.writeHeader(header);

            // write the customer lists
            listWriter.write(toFile, "\n");


        } finally {
            if (listWriter != null) {
                listWriter.close();
            }
        }

    }

    public static List<String> partialReadWithCsvMapReader(String fileName) throws Exception {

        //ICsvMapReader mapReader = null;
        //System.out.println(absoluteFilePath);
        String csvFile = absoluteFilePath + File.separator + fileName + FILE_EXTENSION;
        //List<String> toList = new ArrayList<>();

        try {
            mapReader = new CsvMapReader(new FileReader(csvFile), CsvPreference.STANDARD_PREFERENCE);
            mapReader.getHeader(true); // skip past the header (we're defining our own)
            // not mapping all the columns
            final String[] header = new String[]{"Line Number", "Name", "Sender Number", null, null, null, null, null,
                    null, "POBO Add1", "POBO Add2", "POBO City", "POBO zip", "POBO State", null, null, null, null, null,
                    null,
            };

            // apply some constraints to ignored columns (just because we can)
            final CellProcessor[] processors = new CellProcessor[]{
                    new NotNull(),  // Line number
                    new NotNull(),  // Name
                    new Optional(), // Sender Number
                    new Optional(), // State
                    new Optional(), // Street Address
                    new Optional(), // Street Address 2
                    new Optional(), // City
                    new Optional(), // Zip
                    new Optional(), // State
                    new Optional(), // POBO Add1
                    new Optional(), // POBO Add2
                    new Optional(), // POBO City
                    new Optional(), // POBO zip
                    new Optional(), // POBO State
                    new Optional(), // Email
                    new Optional(), // Work Phone
                    new Optional(), // Fax
                    new Optional(), // Website
                    new Optional(), // National 800 phone
                    new Optional(), // State 800 phone
            };

            //Map<String, Object> map;
            while ((map = mapReader.read(header, processors)) != null) {
                //System.out.println(String.format("Result: %s", customerMap));
                toList.add(String.format("%s", map));
            }

        } finally {
            if (mapReader != null) {
                mapReader.close();
            }
        }
        return toList;
    }

    /**
     * Taking a strings from
     *
     * @param rowForParsing
     * @return
     */
    public List<String> parseEqualAndComma(String rowForParsing) {
        String replaceEdgeCases = rowForParsing
                .replaceAll(", INC", "` INC")
                .replaceAll(", LLC", "` LLC")
                .replaceAll(", LTD", "` LTD")
                .replaceAll(", LP", "` LP")
                .replaceAll(", THE", "` THE");
        String[] parsedComma = replaceEdgeCases.split(",");
        String[] parseEqual;
        String returnToDefault;
        List<String> parsed = new ArrayList<>();
        for (String s : parsedComma) {
            parseEqual = s.trim().split("=");
            returnToDefault = parseEqual[1]
                    .replaceAll("` INC", ", INC")
                    .replaceAll("` LLC", ", LLC")
                    .replaceAll("` LTD", ", LTD")
                    .replaceAll("` LP", ", LP")
                    .replaceAll("` THE", ", THE")
                    .replaceAll("}", "");
            parsed.add(returnToDefault);
        }
        return parsed;
    }

    public Set<String> claParse(Set<String> toParse) {
        Set<String> parsed = new HashSet<>();
        String[] parseEqual;
        String justNumber;
        for (String s : toParse) {
            parseEqual = s.trim().split("=");
            justNumber = parseEqual[1].replaceAll("}", "");
            parsed.add(justNumber);
        }
        return parsed;
    }

}