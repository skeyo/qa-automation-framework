package DriverSetup;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class DriverUtil {

    public static final String DEFAULT_PROPERTIES = "system.properties";

    private static final String OSX = "Mac OS X";
    private static Properties prod;
    private static String operatingSystem = System.getProperty("os.name");

    /**
     * @return the path of ie driver file.
     * Supported Windows
     */
    public static String getIeDriver() {
        if (operatingSystem.equals(OSX)) {
            System.out.println("WRONG OPERATING SYSTEM!!!!");
            return  null;
        } else {
            String path = "drivers/IEDriverServer.2.48.exe";
            return driverPath(path);
        }
    }

    /**
     * @return the path of chrome driver file
     * Supported OS X and Windows
     */
    public static String getChromeDriver() {
        if (operatingSystem.equals(OSX)) {
            String path = "drivers/chromedriver.2.20";
            return driverPath(path);
        } else {
            String path = "drivers/chromedriver.2.20.exe";
            return driverPath(path);
        }
    }

    /**
     * @return load the file system.properties
     */
    public static Properties getProperties() {
        if (prod == null) {
            prod = new Properties();
            try {
                prod.load(DriverUtil.class.getClassLoader().getResourceAsStream(DEFAULT_PROPERTIES));
            } catch (IOException e) {
            }
        }
        return prod;
    }

    /**
     * @param key value of system properties
     * @return get value of key
     */
    public static String getKey(String key) {
        Object obj = getProperties().get(key);
        String value = "";
        if (obj != null)
            value = obj.toString();
        return value;
    }

    private static String driverPath(String path){
        try {
            File driverSel = new File(path);
            if (driverSel.exists()) {
                return driverSel.getAbsolutePath();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }


}