package DriverSetup;

/**
 * Starts driver in local thread
 */

import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class LocalDriverManager {

    private static final int WIDTH = 1200;
    private static final int HEIGHT = 900;
    private static final String REMOTE = "remote";
    private static final String LOCAL = "local";
    private static final String HUB_ADDRESS = "http://46.105.96.171:4444/wd/hub";

    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();

    private static final Logger logger = Logger.getLogger(LocalDriverManager.class);

    public static WebDriver getDriver() {
        return webDriver.get();
    }

    static void setWebDriver(WebDriver driver) {
        webDriver.set(driver);
    }

    private static WebDriver seleniumRemote() {
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setPlatform(Platform.LINUX);

        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(HUB_ADDRESS), capability);
        } catch (MalformedURLException e) {
            logger.info(" Thread: " + Thread.currentThread().getId() + " - Exception is - " + e);
        }
        return driver;
    }

    public static WebDriver getEnv(String environment, String url){
        switch (environment.toLowerCase()){
            case REMOTE:{
                WebDriver driver = seleniumRemote();
                driver.get(url);
                return driver;
            }
            case LOCAL:{
                WebDriver driver = LocalDriverManager.getDriver();
                driver.manage().window().setSize(new Dimension(WIDTH,HEIGHT));
                driver.get(url);
                return  driver;
            }default: //will be used for tests without browser
        }
        return null;
    }
}
