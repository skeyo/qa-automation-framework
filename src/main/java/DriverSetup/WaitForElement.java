package DriverSetup;

import com.google.common.base.Predicate;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Wait for element to be loaded
 */
public class WaitForElement implements IWaitForElement {

    final static Logger logger = Logger.getLogger(WaitForElement.class);
    private static final int WAIT_TIME = 20;
    private static final int RETRY_TIME = 2;

    @Override
    public int getWaitTime() {
        return WAIT_TIME;
    }

    /**
     * Waiting for element to be shown
     *
     * @param driver   instance of webdriver
     * @param selector location of element
     */
    @Override
    public void waitForElement(WebDriver driver, By selector) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(WAIT_TIME, TimeUnit.SECONDS)
                    .pollingEvery(RETRY_TIME, TimeUnit.SECONDS)
                    .ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);

            wait.until(ExpectedConditions.elementToBeClickable(selector));
        } catch (NoSuchElementException | StaleElementReferenceException | TimeoutException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + selector);
        }
    }

    @Override
    public void waitForElementMs(WebDriver driver, By selector) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(WAIT_TIME, TimeUnit.SECONDS)
                    .pollingEvery(RETRY_TIME, TimeUnit.MICROSECONDS)
                    .ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);

            wait.until(ExpectedConditions.elementToBeClickable(selector));
        } catch (NoSuchElementException | StaleElementReferenceException | TimeoutException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + selector);
        }
    }

    /**
     * Waiting for element to be shown
     *
     * @param driver  instance of webdriver
     * @param element webdriver element
     */
    @Override
    public void waitForElement(WebDriver driver, WebElement element) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(WAIT_TIME, TimeUnit.SECONDS)
                    .pollingEvery(RETRY_TIME, TimeUnit.SECONDS)
                    .ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);

            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (NoSuchElementException | StaleElementReferenceException | TimeoutException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + element);
        }
    }

    /**
     * Waiting for text to be shown
     * Useful in situations where we are waiting for text to be visible in element
     *
     * @param driver   instance of webdriver
     * @param text     text that we are waiting for
     * @param selector location of the element
     */
    @Override
    public void waitForText(WebDriver driver, String text, By selector) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(WAIT_TIME, TimeUnit.SECONDS)
                    .pollingEvery(RETRY_TIME, TimeUnit.MICROSECONDS)
                    .ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);

            wait.until(ExpectedConditions.textToBePresentInElementLocated(selector, text));
            logger.info("Thread: " + Thread.currentThread().getId() + " - Waiting for element - " + selector
                    + " - To get text - " + text);
        } catch (TimeoutException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + selector);
        }
    }

    /**
     * Retry to click on element 5 times
     *
     * @param driver   instance of webdriver
     * @param selector location of the element
     */
    @Override
    @Beta
    public void retryElement(WebDriver driver, By selector) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(WAIT_TIME, TimeUnit.SECONDS)
                    .pollingEvery(RETRY_TIME, TimeUnit.SECONDS)
                    .ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);

            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
        } catch (StaleElementReferenceException | TimeoutException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + selector);
        }
    }

    /**
     * Wait for element to become invisible
     *
     * @param driver   instance of webdriver
     * @param selector location of the element to become invisible
     */
    @Override
    @Beta
    public void waitToBeInvisible(WebDriver driver, By selector) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(WAIT_TIME, TimeUnit.SECONDS)
                    .pollingEvery(RETRY_TIME, TimeUnit.SECONDS)
                    .ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);

            wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
        } catch (StaleElementReferenceException | TimeoutException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + selector);
        }
    }

    @Override
    public void visibilityOfAllElements(WebDriver driver, By selector) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(WAIT_TIME, TimeUnit.SECONDS)
                    .pollingEvery(RETRY_TIME, TimeUnit.SECONDS)
                    .ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);

            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector));
        } catch (StaleElementReferenceException | TimeoutException e) {
            logger.error("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + selector);
        }
    }

    @Override
    @Beta
    public void waitForColourToBeChanged(WebDriver driver, By selector)  {

        FluentWait<WebDriver> wait = new FluentWait<>(driver);
        wait.pollingEvery(WAIT_TIME,  TimeUnit.MILLISECONDS);
        wait.withTimeout(RETRY_TIME, TimeUnit.SECONDS);
        wait.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class); //make sure that this exception is ignored

        Predicate<WebDriver> predicate = arg0 -> {
            try {
                Thread.sleep(600);
            } catch (InterruptedException e) {
                logger.error(" Thread: " + Thread.currentThread().getId() + " - Amount wait exception - " + e);
            }
            String color = arg0.findElement(selector).getCssValue("background-color");
            if(color.equals("rgba(255, 255, 255, 1)")) //Default background rgb colour
            {
                return true;
            }
            return false;
        };
        wait.until(predicate);
    }

    @Override
    @Beta
    public void explicitWait(WebDriver driver, By selector) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 4);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
        } catch (org.openqa.selenium.TimeoutException e) {
            logger.info("Thread: " + Thread.currentThread().getId() + " - No errors on location: " + selector);
        }
    }


}
