package DriverSetup;

import org.openqa.selenium.Beta;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by nikolajosifovic on 5/4/15.
 */
public interface IWaitForElement {

    int getWaitTime();

    void waitForElement(WebDriver driver, By selector);

    void waitForElementMs(WebDriver driver, By selector);

    void waitForElement(WebDriver driver, WebElement element);

    void waitForText(WebDriver driver, String text, By selector);

    @Beta
    void retryElement(WebDriver driver, By selector);

    @Beta
    void waitToBeInvisible(WebDriver driver, By selector);

    void visibilityOfAllElements(WebDriver driver, By selector);

    void waitForColourToBeChanged(WebDriver driver, By selector);

    @Beta
    void explicitWait(WebDriver driver, By selector);
}
