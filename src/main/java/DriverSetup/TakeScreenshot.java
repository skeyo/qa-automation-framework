package DriverSetup;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class TakeScreenshot {

    private static ResourceBundle folders = ResourceBundle.getBundle("folders", Locale.US);
    private String screenshotDirectoryPath;
    private File file;
    private String absoluteFilePath;

    /**
     * Constructor for file path
     */
    public TakeScreenshot() {
        screenshotDirectoryPath = this.folders.getString("screenshotDirectory");
        file = new File(screenshotDirectoryPath);
        absoluteFilePath = file.getAbsolutePath();
    }

    private boolean createFile(File screenshot) {

        boolean fileCreated = false;

        if (screenshot.exists()) {
            fileCreated = true;
        } else {
            File parentDirectory = new File(screenshot.getParent());
            if (parentDirectory.exists() || parentDirectory.mkdirs()) {
                try {
                    fileCreated = screenshot.createNewFile();
                } catch (IOException errorCreatingScreenshot) {
                    errorCreatingScreenshot.printStackTrace();
                }
            }
        }
        return fileCreated;
    }

    private void writeScreenshotToFile(WebDriver driver, File screenshot) {
        try {
            FileOutputStream screenshotStream = new FileOutputStream(screenshot);
            screenshotStream.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
            screenshotStream.close();
        } catch (IOException unableToWriteScreenshot) {
            System.err.println("Unable to write " + screenshot.getAbsolutePath());
            unableToWriteScreenshot.printStackTrace();
        }
    }


    public void onTestFailure(String name, WebDriver driver) {
        String screenshotAbsolutePath = absoluteFilePath + File.separator +
                System.currentTimeMillis() + "-" + name + "-Thread-" + Thread.currentThread().getId() + ".png";
        File screenshot = new File(screenshotAbsolutePath);
        if (createFile(screenshot)) {
            try {
                writeScreenshotToFile(driver, screenshot);
            } catch (ClassCastException weNeedToAugmentOurDriverObject) {
                writeScreenshotToFile(new Augmenter().augment(driver), screenshot);
            }
            System.out.println("Written screenshot to " + screenshotAbsolutePath);
        } else {
            System.err.println("Unable to create " + screenshotAbsolutePath);
        }
    }
}