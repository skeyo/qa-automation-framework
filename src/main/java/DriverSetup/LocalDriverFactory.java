package DriverSetup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

/**
 * Select webdriver that is passed from suite XML
 */
class LocalDriverFactory {
    private static final String FIREFOX = "firefox";
    private static final String CHROME = "google-chrome";
    private static final String SAFARI = "safari";
    private static final String IE = "ie";
    private static final String HTML = "html";

    static WebDriver createInstance(String browser) {
        WebDriver driver = null;

        switch (browser.toLowerCase()) {
            case FIREFOX: {
                driver = new FirefoxDriver();
                break;
            }
            case CHROME: {
                System.setProperty("webdriver.chrome.driver", DriverUtil.getChromeDriver());
                driver = new ChromeDriver();
                break;
            }
            case SAFARI: {
                driver = new SafariDriver();
                break;
            }
            case HTML: {
                driver = new HtmlUnitDriver(true);
                break;
            }
            case IE: {
                System.setProperty("webdriver.ie.driver", DriverUtil.getIeDriver());
                DesiredCapabilities caps = new DesiredCapabilities();
                driver = new InternetExplorerDriver(caps);
                break;
            }
            default: //will be used for tests without browser
        }
        return driver;
    }
}