package DriverSetup;

/**
 * Listener for webdriver
 */

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class WebDriverListener implements IInvokedMethodListener {

    final static Logger logger = Logger.getLogger(WebDriverListener.class);
    private boolean failed = false;

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            String browser = method.getTestMethod().getXmlTest().getLocalParameters().get("browser");
            String env = method.getTestMethod().getXmlTest().getParameter("environment");
            if(env.equalsIgnoreCase("local")) {
                WebDriver driver = LocalDriverFactory.createInstance(browser);
                LocalDriverManager.setWebDriver(driver);
            }
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            WebDriver driver = LocalDriverManager.getDriver();
            if (driver != null) {
                if (driver instanceof HtmlUnitDriver) {
                    logger.info("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + "HTML DRIVER");
                    driver.quit();
                }
                if (!method.getTestResult().isSuccess() && !(driver instanceof HtmlUnitDriver)) {
                    TakeScreenshot screenshot = new TakeScreenshot();
                    screenshot.onTestFailure(method.getTestMethod().getMethodName(), driver);
                }
                logger.info("Thread: " + Thread.currentThread().getId() + " - " + driver.getCurrentUrl() + " - " + "Driver Closed");
                driver.quit();
            }
        }
    }
}
