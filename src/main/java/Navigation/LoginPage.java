package Navigation;



import Common.WebInteractionsActions.*;
import DriverSetup.IWaitForElement;
import DriverSetup.WaitForElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


/**
 * Created by nikol on 11/26/2015.
 */
public class LoginPage {

        private By emailInputField = By.name("email");
        private By passwordInputField = By.name("password");
        private By signInButton = By.cssSelector(".butt.bBlue.fr");
        private By statusMsg = By.cssSelector("#status > li");
        private By application = By.id("application");

    final static Logger logger = Logger.getLogger(LoginPage.class);
    private IClickInteractions clickInteractions = new ClickInteractions();
    private IGetInfo getInfo = new GetInfo();
    private CommonInteractions commonInteractions = new CommonInteractions();
    private IWaitForElement waitForElement = new WaitForElement();

    public void enterEmail(WebDriver driver, String email){
        commonInteractions.clickSendKeys(driver, email, emailInputField);
    }

    public void enterPassword(WebDriver driver, String password){
        commonInteractions.clickSendKeys(driver, password, passwordInputField);
    }

    public void clickSubmit(WebDriver driver){
        clickInteractions.clickOnElement(driver, signInButton);
        logger.info(" Thread: " + Thread.currentThread().getId() + " - Clicking on Element - " + signInButton);
    }

    public void isLoggedIn(WebDriver driver){
        waitForElement.waitForElementMs(driver, statusMsg);
        //System.out.println(driver.findElement(statusMsg).getText());
        if(driver.findElement(statusMsg).getText().equalsIgnoreCase("Incorrect email or password")) {
            logger.error(" Thread: " + Thread.currentThread().getId() + " - Login Error - " + getInfo.getText(driver, statusMsg));
        }
        else
            //waitForElement.waitForText(driver, "Login successful, redirecting...", statusMsg);
            waitForElement.waitForElement(driver, application);
            logger.info(" Thread: " + Thread.currentThread().getId() + " - Login Successful");
    }
}
